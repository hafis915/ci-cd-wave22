const app = require('../app')
const {Name}= require('../models')
const request = require('supertest')

const name1 = {
    name : 'test1',
    age : 2
}

const name2 = {
    name : 'test2',
    age : 3
}

const name3 = {
    name : 'test3',
    age : 4
}


const createNewData = {
    name : 'newData',
    age : 5
}

let _id 

jest.setTimeout(10000)

beforeAll(async () => {
    const deleteData =  await Name.create(name1)
    await Name.create(name2)
    await Name.create(name3)

    _id = deleteData.dataValues.id  
})

afterAll((done) => {
    Name.destroy({where : {}}).then(res => {
        done()
    })
})

describe('Positif case GET /names' , () => {
    it('Success Get All Data', (done) => {
        request(app)
        .get('/names')
        .then((res) =>{
            const { body, status } = res
            const index1 = body[0]
            const index2 = body[1]
            const index3 = body[2]
            expect(status).toBe(200)
            expect(index1.id).toBeTruthy()
            expect(index1.name).toBe(name1.name)
            expect(index1.age).toBe(name1.age)
            expect(index2.name).toBe(name2.name)
            expect(index2.age).toBe(name2.age)
            expect(index3.name).toBe(name3.name)
            expect(index3.age).toBe(name3.age)
            done()
        })
    })

    it('success Create new Data', (done) => {
        request(app)
        .post('/names')
        .send(createNewData)
        .set('Accept', 'application/json')
        .then(res => {
            const {status , body} = res
            expect(status).toBe(201)
            expect(body.name).toBe(createNewData.name)
            expect(body.age).toBe(createNewData.age)
            done()
        })
    })

    it('success delete data', (done) =>{

        request(app)
        .delete(`/names/${_id}`)
        .then(res => {
            const {status, body} = res
            expect(status).toBe(200)
            done()
        })
    }) 


})


