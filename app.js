const express = require('express')
const app = express()
const {Name} = require("./models")
const cors = require('cors')
require('dotenv').config()

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.get('/', async (req,res) => {
    res.send('HELLO WORLD FROM CI CD')
})

app.get('/names', async (req,res) => {
    const data = await Name.findAll({
        order : ['id']
    })
    res.status(200).json(data)
})

app.post('/names', async (req,res) => {
    console.log(req.body)
    const { name , age} = req.body
    const newData = await Name.create({
        name,age
    })

    res.status(201).json(newData)
})


app.delete('/names/:id', async(req,res) => {
    const id = req.params.id
    await Name.destroy({
        where : {
            id 
        }
    })
    res.status(200).json({
        message : 'success'
    })
})


module.exports = app